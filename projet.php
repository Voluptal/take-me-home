<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Take Me Home</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Raleway:400,300,700,900" rel="stylesheet">


  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">


  <link href="css/style.css" rel="stylesheet">
</head>

<body data-spy="scroll" data-offset="80" data-target="#thenavbar">

  <!-- Fixed navbar -->
  <div id="thenavbar" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-brand" href="http://iparla.iutbayonne.univ-pau.fr/~flanfrit/WS/"><i class="fa fa-flag"></i></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="http://iparla.iutbayonne.univ-pau.fr/~flanfrit/WS/" class="smoothscroll">Home</a></li>
          <li><a href="api.html" class="smoothscroll">A.P.I</a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>

  <div id="hello">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 centered">
          <h1>Take Me Home</h1>
          <h2>Find anthem with IP</h2>
		  <br><br><br>
<?php
	if (isset($_POST['ip'])) {
	
		$ip = $_POST['ip'];
		$ip = str_replace(" ", "", $ip);
		if ($ip == "") { 
			header('Location:index.html');
			return;
		}
		$url = "http://iparla.iutbayonne.univ-pau.fr/~flanfrit/WS/server.php?method=getHymne&ip=".$ip;
		$json = file_get_contents($url);
		$docJson = json_decode($json);
		if(isset($docJson->pays)) {
			$pays = $docJson->pays;
			$videoId = $docJson->videoId;
			$urlVideo = "https://www.youtube.com/embed/".$videoId."?autoplay=1";
			echo ("<p>L'ip <strong>".$ip."</strong> correspond au pays <strong>".$pays."</strong> ! <br>Vous écoutez actuellement l'hymne de ce pays :D</p>");
			echo ('<br><img src="https://risibank.fr/cache/stickers/d245/24549-full.gif">');
			echo ('<iframe width="0" height="0" src="'.$urlVideo.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
		} else {
			echo ("<p>".$docJson->message."</p>");
			echo ('<br><img src="https://risibank.fr/cache/stickers/d125/12554-full.png">');
		}
	}
?>
</div>
        <!-- /col-lg-8 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /hello -->
 
  <div id="copyrights">
    <div class="container">
      <p>
        &copy; Copyrights <strong>Take Me Home</strong>. All Rights Reserved
      </p>
      <div class="credits">
        <!--
          You are NOT allowed to delete the credit link to TemplateMag with free version.
          You can delete the credit link only if you bought the pro version.
          Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/bolt-bootstrap-agency-template/
          Licensing information: https://templatemag.com/license/
        -->
        Created by Nicolas GIMBERT & Florian LANFRIT
      </div>
    </div>
  </div>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
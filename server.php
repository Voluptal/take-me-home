<?php
	
	if(isset($_GET['method'])){
	
	$method = $_GET['method'];

	if($method == "getHymne") {
		
		
		if(isset($_GET['ip'])){
		
			$ip = $_GET['ip'];
			
			$pays = trouverPays($ip);
			if ($pays != null) {
				$idVideo = getVideoId($pays);
				$arr = array('pays' => $pays, 'videoId' => $idVideo);
				echo(json_encode($arr));
			} else {
				$arr = array('code' => 'Erreur', 'message' => 'Veuillez saisir une ip valide');
				echo(json_encode($arr));
			}
		
		
		} else {
			$arr = array('code' => 'Erreur', 'message' => 'Merci de renseigner une ip dans URI');
			echo(json_encode($arr));
		}
	} else {
		$arr = array('code' => 'Erreur', 'message' => 'Methode inconnue');
		echo(json_encode($arr));
	}
	
	} else {
		$arr = array('code' => 'Erreur', 'message' => 'Erreur de syntaxe. Il manque la methode');
		echo(json_encode($arr));
	}
	
	function getVideoId($p) {
		
		$pays=str_replace(' ','+',$p);
		$key = "AIzaSyCcNMKAkuolyvGWv1K_E46abKGoNQ1ot_Y";
			
		$query = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=hymne".$pays."&type=video&key=".$key;
		$json = file_get_contents($query);
		
		$j = json_decode($json);
		
		$videoID = $j->{'items'}[0]->{'id'}->{'videoId'};
		
		return($videoID);
	
	
	}
	
	function trouverPays($ip) {
		$apiKey = "1c5c5660b50f112973e4f56eef78288d";
		$url = "http://api.ipstack.com/".$ip."?access_key=".$apiKey."&fields=country_name";
		$json = file_get_contents($url);
		$jreturn = json_decode($json);
		if(isset($jreturn->country_name)) {
			return($jreturn->country_name);
		} else {
			return null;
		}
	}

?>